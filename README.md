# Network Priority
A DLL that lets you control an object's network priority.

## Usage
Set `%obj.networkPriority` to some float value, e.g. `%obj.networkPriority = 999;`.  The field must be set during object creation or in its `::onAdd` method if you want it to ghost immediately.  The higher the value, the greater its priority.  By default, objects have a network priority usually less than 5 based on their distance to a client's camera, etc.  When an object is deleted, it is given a priority of 10000 which cannot be changed.  Set the field to an empty string to make the object use its normal priority.
